

const numbers = [2,3,4,5,6,7];

const square = function(number){
    return number * number;
}
const ans = numbers.map(square);

console.log(ans);

const ans1 = numbers.map(function(number){
    return number * number;
});

console.log(ans1);

const ans2 = numbers.map((number) => {
        return number * number;
});

console.log(ans2);


const users = [
    {firstName : "Amay" ,lastName : "san"},
    {firstName : "may" ,lastName : "an"},
    {firstName : "ay" ,lastName : "n"},
];

const ans4 = users.map((user) => {
    return user.firstName;
});

console.log(ans4);
